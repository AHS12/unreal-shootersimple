// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ShooterSimpleGameModeBase.h"
#include "KillEmAllGameMode.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERSIMPLE_API AKillEmAllGameMode : public AShooterSimpleGameModeBase
{
	GENERATED_BODY()

public:
	virtual	void PawnKilled(APawn* KilledPawn) override;
private:
	void EndGame(bool bIsPlayerWinner);
};
