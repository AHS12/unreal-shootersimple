// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ShooterSimpleGameModeBase.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERSIMPLE_API AShooterSimpleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	virtual	void PawnKilled(APawn* KilledPawn);

};
