// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"

#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#define OUT

// Sets default values
AGun::AGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("ROOT"));
	SetRootComponent(Root);
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);

}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();


}

AController* AGun::GetOwnerController() const
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (!OwnerPawn) { return nullptr; }
	//UE_LOG(LogTemp, Warning, TEXT("Owner Found"));
	return OwnerPawn->GetController();

}

bool AGun::GunTrace(FHitResult& HitResult, FVector& ShotDirection)
{


	FVector OwnerLocation;
	FRotator OwnerRotation;
	AController* OwnerController = GetOwnerController();
	if (!OwnerController) { return false; }
	OwnerController->GetPlayerViewPoint(OUT OwnerLocation, OUT OwnerRotation);
	OUT ShotDirection = -OwnerRotation.Vector();
	/*UE_LOG(LogTemp, Warning, TEXT("Out Location : %s"), *OwnerLocation.ToString());
	UE_LOG(LogTemp, Warning, TEXT("Out Rotation : %s"), *OwnerRotation.ToString());*/

	FVector End = OwnerLocation + OwnerRotation.Vector() * MaxRange;


	//DrawDebugCamera(GetWorld(), OwnerLocation, OwnerRotation, 90, 2.0, FColor::Red, true);
	//DrawDebugLine(GetWorld(), OwnerLocation, End, FColor::Red, true);


	//FHitResult HitResult;
	FCollisionQueryParams ShotIgnoreCollisionParams;
	ShotIgnoreCollisionParams.AddIgnoredActor(this);
	ShotIgnoreCollisionParams.AddIgnoredActor(GetOwner());
	return GetWorld()->LineTraceSingleByChannel(OUT HitResult, OwnerLocation, End, ECollisionChannel::ECC_GameTraceChannel1, ShotIgnoreCollisionParams);

}

void AGun::PullTrigger()
{
	//UE_LOG(LogTemp, Warning, TEXT("Trigger Pulled!"));

	/*--Muzzle sound & effect start--*/
	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));
	/*--Muzzle sound & effect end--*/
	FHitResult HitResult;
	FVector ShotDirection;
	bool bSuccess = GunTrace(HitResult, ShotDirection);
	if (bSuccess) {
		//DrawDebugPoint(GetWorld(), HitResult.Location, 20, FColor::Red, true);
		/*--Impact sound & effect start--*/
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, HitResult.Location, ShotDirection.Rotation());
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, HitResult.Location, ShotDirection.Rotation());
		/*--Impact sound & effect end--*/
		AActor* HitActor = HitResult.GetActor();
		if (HitActor) {
			FPointDamageEvent DamageEvent(Damage, HitResult, ShotDirection, nullptr);
			AController* OwnerController = GetOwnerController();
			HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
			//UE_LOG(LogTemp, Warning, TEXT("Shot Hit.Damage: %f"), Damage);
		}
	}
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

