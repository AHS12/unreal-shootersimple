// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterPlayerController.h"
#include "TimerManager.h"
#include "Blueprint/UserWidget.h"
#include "ShooterAIController.h"
#include "EngineUtils.h"


void AShooterPlayerController::BeginPlay()
{
	Super::BeginPlay();
	HUD = CreateWidget(this, HUDClass);
	if (HUD)
	{
		HUD->AddToViewport();
	}
	GameaStartMsg = CreateWidget(this, GameStartScreenClass);
	if (GameaStartMsg)
	{
		GameaStartMsg->AddToViewport();
		GetWorldTimerManager().SetTimer(ShowQuickMsgTimer, this, &AShooterPlayerController::ClearWelcomeMsg, 5.0f, false);
	}
}

void AShooterPlayerController::ClearWelcomeMsg()
{
	GetWorldTimerManager().ClearTimer(ShowQuickMsgTimer);
	if (GameaStartMsg) {
		GameaStartMsg->RemoveFromViewport();
	}
}


int32 AShooterPlayerController::GetAliveEnemyCount() const
{
	int32 Count = 0;
	for (AShooterAIController* Controller : TActorRange<AShooterAIController>(GetWorld()))
	{
		if (!Controller->IsDead())
		{
			Count++;
		}

	}

	return Count;
}

void AShooterPlayerController::GameHasEnded(class AActor* EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);

	//UE_LOG(LogTemp, Warning, TEXT("Game Ended.Msg From Player Controller."));
	HUD->RemoveFromViewport();
	if (bIsWinner)
	{
		UUserWidget* WinScreen = CreateWidget(this, WinScreenClass);
		if (WinScreen)
		{
			WinScreen->AddToViewport();
		}
	}
	else
	{
		UUserWidget* LooseScreen = CreateWidget(this, LooseScreenClass);
		if (LooseScreen)
		{
			LooseScreen->AddToViewport();
		}
	}

	GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);
}