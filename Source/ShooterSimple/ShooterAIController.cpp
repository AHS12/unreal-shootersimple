// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAIController.h"

#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "ShooterCharacter.h"



void AShooterAIController::BeginPlay()
{
	Super::BeginPlay();

	PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	if (AiBehavior) {
		RunBehaviorTree(AiBehavior);


		GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), GetPawn()->GetActorLocation());
	}
}

// Called every frame
void AShooterAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	/*if (LineOfSightTo(PlayerPawn)) {

		SetFocus(PlayerPawn);
		MoveToActor(PlayerPawn, AcceptanceRadius);

	}
	else {
		ClearFocus(EAIFocusPriority::Gameplay);
		StopMovement();

	}*/

	//if (LineOfSightTo(PlayerPawn))
	//{
	//	//setting player location
	//	GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"), PlayerPawn->GetActorLocation());
	//	//seting last known player location
	//	GetBlackboardComponent()->SetValueAsVector(TEXT("LastKnownPlayerLocation"), PlayerPawn->GetActorLocation());

	//}
	//else {
	//	//clear player location
	//	GetBlackboardComponent()->ClearValue(TEXT("PlayerLocation"));
	//}


}

bool AShooterAIController::IsDead() const
{

	AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter>(GetPawn());
	if (ControlledCharacter) {
		return ControlledCharacter->IsDead();
	}

	return true;
}

