// Copyright Epic Games, Inc. All Rights Reserved.

#include "ShooterSimple.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ShooterSimple, "ShooterSimple" );
