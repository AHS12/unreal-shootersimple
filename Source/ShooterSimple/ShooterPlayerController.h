// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterPlayerController.generated.h"

/**
 *
 */
UCLASS()
class SHOOTERSIMPLE_API AShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void GameHasEnded(class AActor* EndGameFocus = nullptr, bool bIsWinner = false) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


private:

	UPROPERTY(EditAnywhere)
		TSubclassOf< class UUserWidget> HUDClass;

	UPROPERTY(EditAnywhere)
		TSubclassOf< class UUserWidget> GameStartScreenClass;

	UPROPERTY(EditAnywhere)
		TSubclassOf< class UUserWidget> LooseScreenClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf< class UUserWidget> WinScreenClass;

	UPROPERTY(EditAnywhere)
		float RestartDelay = 5.f;

	FTimerHandle RestartTimer;
	FTimerHandle ShowQuickMsgTimer;

	UUserWidget* HUD;
	UUserWidget* GameaStartMsg;

	void ClearWelcomeMsg();

	UFUNCTION(BlueprintPure)
		int32 GetAliveEnemyCount() const;
};
